package com.example.memorycardgame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class ActivityResult : AppCompatActivity() {

    private lateinit var scoreText: TextView
    private lateinit var resetButton: Button
    private lateinit var shareButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MemoryCardGame)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        scoreText = findViewById(R.id.textViewScore)
        resetButton = findViewById(R.id.buttonReset)
        shareButton = findViewById(R.id.buttonShare)

        var bundle:Bundle? = intent.extras
        var score: Int? = bundle?.getInt("score")
        scoreText.setText("SCORE " + score.toString())

        var lastlevel: Int? = bundle?.getInt("level")

        resetButton.setOnClickListener {
            if (lastlevel == 0){
                Intent(this, Lvl1OTRO::class.java)
            }
            else if (lastlevel == 1){
                Intent(this, HardLevel::class.java)
            }
        }
    }
}