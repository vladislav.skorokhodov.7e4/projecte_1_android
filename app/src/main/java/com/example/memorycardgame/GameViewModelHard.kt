package com.example.memorycardgame

import androidx.lifecycle.ViewModel


class GameViewModelHard: ViewModel() {

    var lev1images = arrayOf(
        R.drawable.card_chariot,
        R.drawable.card_star,
        R.drawable.card_chariot,
        R.drawable.card_star,
        R.drawable.death,
        R.drawable.death,
        R.drawable.thewardo,
        R.drawable.thewardo
    )

    var cartes = mutableListOf<Carta>()

    init {
        setDataModel()
    }

    var lastCard: Int = -1
    var score: Int = 0
    var animation:Boolean = false
    var movements: Int = 0
    private fun setDataModel() {
        lev1images.shuffle()
        for (i in 0..7) {
            cartes.add(Carta(i, lev1images[i]))
        }
    }

    fun girarCarta(idCarta: Int) : Int {
        cartes[idCarta].girada = true
        return cartes[idCarta].resId
    }

    fun rEGirar(idCarta: Int): Int{
        cartes[idCarta].girada = false
        return R.drawable.carta
    }

    fun resetEstatJoc() {
        for (i in 0..7) {
            cartes[i].girada = false
        }
    }


    fun estatCarta(idCarta: Int): Int {
        if(cartes[idCarta].girada) return cartes[idCarta].resId
        else return R.drawable.carta
    }

    fun wingame():Boolean{
        cartes.forEach{c->if(!c.girada) return false}
        return true
    }

    fun compararCartes(idCarta: Int) : Boolean{
        return cartes[idCarta].resId == cartes[lastCard].resId
    }

    fun modifyScore(modificator:Int){
        score += modificator
    }
}