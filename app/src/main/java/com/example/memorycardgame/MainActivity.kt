package com.example.memorycardgame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*

class MainActivity : AppCompatActivity() {

    private lateinit var buttonStart: Button
    private lateinit var buttonHelp: TextView
    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.Theme_MemoryCardGame)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var intent:Intent
        var sel=0
        /*val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.Difficulties,
            android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)*/

        buttonStart = findViewById(R.id.play_button)



        val spinner: Spinner = findViewById(R.id.spinner)
        val listDifficulties = resources.getStringArray(R.array.Difficulties)
        spinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, listDifficulties)
        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                sel = p2
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                sel = 1
            }

        }

        buttonStart.setOnClickListener {
            when (sel){
                0->intent = Intent(this, Lvl1OTRO::class.java)
                1->intent = Intent(this, HardLevel::class.java)
                else->intent = Intent(this, Lvl1OTRO::class.java)
            }
            startActivity(intent)
        }

    }
}