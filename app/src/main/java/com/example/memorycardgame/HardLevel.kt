package com.example.memorycardgame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class HardLevel : AppCompatActivity() {

    private lateinit var image1: ImageView
    private lateinit var image2: ImageView
    private lateinit var image3: ImageView
    private lateinit var image4: ImageView
    private lateinit var image5: ImageView
    private lateinit var image6: ImageView
    private lateinit var image7: ImageView
    private lateinit var image8: ImageView

    private lateinit var pauseButton: Button

    private lateinit var viewModel: GameViewModelHard

    private lateinit var buttons: Array <ImageView>

    private lateinit var movemennt: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hard_level)
        viewModel = ViewModelProvider(this).get(GameViewModelHard::class.java)

        movemennt = findViewById(R.id.movements_counter)

        image1 = findViewById(R.id.card_image1)
        image2 = findViewById(R.id.card_image2)
        image3 = findViewById(R.id.card_image3)
        image4 = findViewById(R.id.card_image4)
        image5 = findViewById(R.id.card_image5)
        image6 = findViewById(R.id.card_image6)
        image7 = findViewById(R.id.card_image7)
        image8 = findViewById(R.id.card_image8)

        val images: MutableList<Int> = mutableListOf(
            R.drawable.card_star,
            R.drawable.card_chariot,
            R.drawable.death,
            R.drawable.thewardo
        )

        buttons = arrayOf(image1, image2, image3, image4, image5, image6, image7,image8)

        images.shuffle()

        for (i in 0..7) {
            buttons[i].setOnClickListener {
                if(!viewModel.animation){
                    viewModel.movements++
                    println(viewModel.movements)
                    buttons[i].setImageResource(viewModel.girarCarta(i))
                    if(viewModel.lastCard==i){
                        viewModel.lastCard = viewModel.lastCard
                    }else if (viewModel.lastCard < 0 ){
                        viewModel.lastCard = i
                    } else{
                        if (!viewModel.compararCartes(i)){
                            viewModel.animation = true
                            val hideCards = object: CountDownTimer(500, 500) {
                                override fun onTick(millisUntilFinished: Long) {}

                                override fun onFinish() {
                                    buttons[i].setImageResource(viewModel.rEGirar(i))
                                    buttons[viewModel.lastCard].setImageResource(viewModel.rEGirar(viewModel.lastCard))
                                    viewModel.modifyScore(-300)
                                    viewModel.lastCard = -1
                                    viewModel.animation = false
                                }
                            }
                            hideCards.start()
                        }else{
                            viewModel.modifyScore(1000)
                            viewModel.lastCard = -1
                        }

                        /*if (!viewModel.compararCartes(i)){
                            buttons[i].setImageResource(viewModel.rEGirar(i))
                            buttons[viewModel.lastCard].setImageResource(viewModel.rEGirar(viewModel.lastCard))
                        }*/
                        //viewModel.lastCard = -1

                    }
                    if (viewModel.wingame()){
                        var intent = Intent(this, ActivityResult::class.java)
                        intent.putExtra("score", viewModel.score)
                        intent.putExtra("level", 1)
                        startActivity(intent)

                    }
                }
                updateUI()
            }

        }

        updateUI()


        pauseButton = findViewById(R.id.pauseButton)
        pauseButton.setOnClickListener{
            viewModel.score = 0
            viewModel.lastCard = -1
            viewModel.movements = 0
            viewModel.resetEstatJoc()
            updateUI()
        }




    }

    fun updateUI() {
        for (i in 0..7 ) {
            buttons[i].setImageResource(viewModel.estatCarta(i))
        }
        movemennt.setText("MOVEMENTS: " + viewModel.movements.toString())
    }

}